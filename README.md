# ubuntu-nginx

Add nginx to ubuntu-base image, for use in other projects

# Build Your Own

1. git clone https://gitlab.com/doritoes/ubuntu-nginx
2. cd ubuntu-nginx
3. docker build -t ubuntu-nginx:latest .

# Raspberry Pi 3 (and other compatible ARM systems)

## 32-bit OS arm32v6
Pi 1, original Pi 2 B, Compute Module 1, Pi Zero, Pi Zero W

Ubuntu does not have an arm32v6 image anymore. Look to arm32v6/apline.

armhf organization is deprecated in favor of the more-specific arm32v7 and arm32v6 organizations. [link](https://github.com/docker-library/official-images#architectures-other-than-amd64)

## 32-bit OS arm32v7/arm32v8
1. git clone https://gitlab.com/doritoes/ubuntu-nginx
2. cd ubuntu-nginx
3. docker build -t ubuntu-nginx:armhf -f Dockerfile.arm32v7 .

## 64-bit OS arm64v8
Raspberry Pi 2B and later running 64-bit OS
1. git clone https://gitlab.com/doritoes/ubuntu-nginx
2. cd ubuntu-nginx
3. docker build -t ubuntu-nginx:arm64v8 -f Dockerfile.arm64v8 .

# Use In Another Image

 * FROM doritoes/ubuntu-nginx:latest
 * FROM doritoes/ubuntu-nginx:arm32v7
 * FROM doritoes/ubuntu-nginx:arm64v8
